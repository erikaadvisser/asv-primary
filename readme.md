ASV java code
=============

Development
-----------
add the /src/main/groovy to the classpath by hand in your IDE.

Building ASV
--------
mvn clean package

-> rename "asv-boot-1.0-SNAPSHOT.war" to "asv.war" and copy to src/docker/asv


Dockers
=======


Front end docker
-----------------
The FE server is Apache as a reverse proxy

cd $PROJECT_ROOT
cd src/docker/apache
docker build -t ervi/proxy .

-> You should now have a docker image ervi/proxy:latest of about 142 MB


Business server
---------------
The 'database' is the server that does the actual work

The docker is a two stage thing:
1. the tomcat docker
2. ASV application added.

cd $PROJECT_ROOT/src/docker/tomcat85
docker build -t tomcat85 .

cd $PROJECT_ROOT/src/docker/asv
docker build -t ervi/tomcat .

-> You should now have a docker image ervi/tomcat:latest of about 345 MB

Configuring it to run
=====================

docker run --name tomcat -v /dev/urandom:/dev/random ervi/tomcat &

docker run -p80:80 --link tomcat:tomcat --name proxy1 ervi/proxy &
docker run -p81:80 --link tomcat:tomcat --name proxy2 ervi/proxy &
docker run -p82:80 --link tomcat:tomcat --name proxy3 ervi/proxy &


The application is now available on the docker-machine on ports 80, 81 and 82.


> The port 80-82 were chosen at random. Other configurations will work as well. The
apache proxies find the backend server via their hosts file, that has an entry for
'tomcat' set by docker.


> The -v /dev/urandom:/dev/random is because Tomcat requires some good randomness for its SSL
> and docker images are not full of random


During the test
===============

- I have had times when the proxy dockers did not come up again with "docker start" after
  they were killed. Just restart them via the full command, e.g.
  docker run -p81:80 --link tomcat:tomcat --name proxy2 ervi/proxy

- The tomcat server can be stopped gracefully with
docker stop
and started again with
docker start -a tomcat


Starting up will take some time (10s+) because the state is being loaded.

After a tomcat restart, it could theoretically be needed to restart a proxy. If the tomcat server
get's a new IP address, the proxies won't be able to find them.

