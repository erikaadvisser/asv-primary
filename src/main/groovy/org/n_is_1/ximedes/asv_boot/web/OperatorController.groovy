package org.n_is_1.ximedes.asv_boot.web

import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.service.data.Database
import org.n_is_1.ximedes.asv_boot.service.queue.RouterQueue
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateAccount
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Controller cluster operator calls
 */
@RestController
public class OperatorController {

    static final def logback = LoggerFactory.getLogger(OperatorController)
    static final def log = new Log()

    @Autowired Config config
    @Autowired Database database
    @Autowired RouterQueue queue

    @RequestMapping(value = '/status/{node}', method = RequestMethod.GET)
    public def status(@PathVariable(value = "node") int node) {
        def response = doCommand(node, "status")
        def status = response.input.readUTF()
        def primary = response.input.readBoolean()
        def running = response.input.readBoolean()
        response.close()

        return [status: status,
                primary: primary,
                running: running ]
    }

    @RequestMapping(value = '/lastid/{node}', method = RequestMethod.GET)
    public def lastid(@PathVariable(value = "node") int node) {
        def response = doCommand(node, "lastid")
        def lastid = response.input.readInt()
        response.close()

        return [lastid: lastid ]
    }
    @RequestMapping(value = '/start/{node}', method = RequestMethod.GET)
    public def start(@PathVariable(value = "node") int node) {
        doSimpleCommand(node, "stop")
        return "ok"
    }

    @RequestMapping(value = '/stop/{node}', method = RequestMethod.GET)
    public def stop(@PathVariable(value = "node") int node) {
        doSimpleCommand(node, "stop")
        return "ok"
    }

    @RequestMapping(value = '/kill/{node}', method = RequestMethod.GET)
    public def kill(@PathVariable(value = "node") int node) {
        doSimpleCommand(node, "kill")
        return "ok"
    }

    @RequestMapping(value = '/clean/{node}', method = RequestMethod.GET)
    public def clean(@PathVariable(value = "node") int node) {
        doSimpleCommand(node, "clean")
        return "ok"
    }

    private void doSimpleCommand(int node, String command) {
        def response = doCommand(node, command)
        response.input.readUTF()
        response.close()
    }



    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Void> defaultErrorHandler(Exception e) throws Exception {
        logback.error("Caught exception thrown by controller", e)
        return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    Response doCommand(int node, String command) {
        int port = config.operatorPorts[node]
        String hostName = config.hostNames[node]
        Socket socket = new Socket(hostName, port)
        DataInputStream input = new DataInputStream(socket.getInputStream())
        DataOutputStream output = new DataOutputStream(socket.getOutputStream())

        output.writeUTF(command)

        return new Response(socket: socket, input: input)
    }

    class Response {
        Socket socket
        DataInputStream input

        def close() {
            try {
                socket.close()
            }
            catch (Exception ignore){
            }
        }
    }

}