package org.n_is_1.ximedes.asv_boot.web

import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.service.data.Database
import org.n_is_1.ximedes.asv_boot.service.queue.RouterQueue
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateAccount
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Controller for all ASV calls.
 */
@RestController
public class MainController {

    static final def logback = LoggerFactory.getLogger(MainController)
    static final def log = new Log()

    @Autowired Config config
    @Autowired Database database
    @Autowired RouterQueue queue

    @RequestMapping(value = "/account2", method = RequestMethod.POST)
    public ResponseEntity createAccount2(@RequestBody String request) {
        log.trace("web received create account")

        int start = request.indexOf(':') +1
        int end = request.indexOf('}', start)
        String content = request.substring(start, end).trim()
        int overDraft = Integer.parseInt(content)

        def accountId = queue.createAccount(overDraft)

        HttpHeaders headers = new HttpHeaders()
        headers.setLocation(new URI("/account/${accountId}"))
        return new ResponseEntity<Void>(headers, HttpStatus.ACCEPTED)
    }

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public ResponseEntity createAccount(@RequestBody CreateAccount request) {
        log.trace("web received create account")
        def accountId = queue.createAccount(request.overdraft)

        HttpHeaders headers = new HttpHeaders()
        headers.setLocation(new URI("/account/${accountId}"))
        return new ResponseEntity<Void>(headers, HttpStatus.ACCEPTED)
    }

    @RequestMapping(value = '/account/{accountId}', method = RequestMethod.GET)
    public def getAccount(@PathVariable(value = "accountId") int accountId) {
        log.trace("web received get account ${accountId}")
        if (!queue.running) {
            throw new RuntimeException("Node not running.")
        }
        Account account = database.getAccount(accountId)

        if (!account) {
            return ResponseEntity.notFound()
        }

        return [accountId: account.id,
                balance: account.balance,
                overdraft: account.overdraft ]

    }

    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public ResponseEntity createTransfer(@RequestBody CreateTransfer request) {
        log.trace("web received create transfer")
        def transferId = queue.transfer(request)
        HttpHeaders headers = new HttpHeaders()
        headers.setLocation(new URI("/transfer/${transferId}"))
        return new ResponseEntity<Void>(headers, HttpStatus.ACCEPTED)
    }

    @RequestMapping(value = '/transfer/{transferId}', method = RequestMethod.GET)
    public def getTransfer(@PathVariable(value = "transferId") int transferId) {
        log.trace("web received get transfer ${transferId}")
        if (!queue.running) {
            throw new RuntimeException("Node not running.")
        }
        Transfer transfer = database.getTransfer(transferId)

        if (!transfer) {
            return ResponseEntity.notFound()
        }

        return [transactionId: transfer.id,
                from         : transfer.from,
                to           : transfer.to,
                amount       : transfer.amount,
                status       : transfer.status ]
    }

    @RequestMapping(value = '/transaction/{transactionId}', method = RequestMethod.GET)
    public def getTransaction(@PathVariable(value = "transactionId") int transactionId) {
        log.trace("web received get transfer ${transactionId}")
        if (!queue.running) {
            throw new RuntimeException("Node not running.")
        }
        Transfer transfer = database.getTransfer(transactionId)

        if (!transfer || transfer.status != Transfer.Status.CONFIRMED) {
            return ResponseEntity.notFound()
        }

        return [transactionId: transfer.id,
                from         : transfer.from,
                to           : transfer.to,
                amount       : transfer.amount ]
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public ResponseEntity health() {
        if (queue.running) {
            return ResponseEntity.ok().build()
        }
        else {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build()
        }
    }

    @RequestMapping(value = "/die", method = RequestMethod.GET)
    public ResponseEntity die() {
        log.info("Node stop / abort (received kill command)")
        Thread.sleep(100)
        System.exit(0)
        return ResponseEntity.ok().build()
    }


    @RequestMapping(value = "/clean", method = RequestMethod.GET)
    public String clean() {
        println "enter clean"

        database.clean()
        database.printBalance()
        return "done"
    }

    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public String printBalance() {
        println "start balance"
        database.printBalance()
        return "end balance"
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Void> defaultErrorHandler(Exception e) throws Exception {
        logback.error("Caught exception thrown by controller", e)
        return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR)
    }

}