package org.n_is_1.ximedes.asv_boot.model

/**
 * An attempt to transfer money from account A to account B. If successful, will result in a Transaction
 */
class Transfer implements Serializable {
    
    enum Status { PENDING, CONFIRMED, INSUFFICIENT_FUNDS, ACCOUNT_NOT_FOUND }

    int id
    int from
    int to
    int amount
    Status status
//    long creationMillis
}
