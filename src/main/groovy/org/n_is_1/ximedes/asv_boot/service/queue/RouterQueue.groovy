package org.n_is_1.ximedes.asv_boot.service.queue

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This queue acts as a router and manager to manage requests to actual queues:
 * - choose the local or remote queue
 * - retry messages if they failed (due to a host not available, or cluster startup)
 * - manage starting and stopping of message processing
 */
@Service
class RouterQueue {
    final def log = new Log()
    final int sleep = 1
    final int sleepMillis = sleep * 1000

    ActionQueue currentQueue = null
    boolean running = false

    synchronized int createAccount(int overdraft) {
        while (true) {
            try {
                if (running && currentQueue) {
                    return currentQueue.createAccount(overdraft, false)
                }
                else {
                    log.trace("No connection yet for 'createAccount' message, retrying in ${sleep} seconds")
                }
            }
            catch (Exception e) {
                // TODO: determine if we need to shut down the queue in this case
                // if we shut down the queue, we also need to notify the cluster that it needs to repair itself,
                // otherwise we end up in a deadlock...
//                running = false
                log.info("Action 'createAccount' failed due to ${e.getMessage()}, retrying in ${sleep} seconds")
            }
            log.info("Action 'createAccount' failed due retrying in ${sleep} seconds")
            Thread.sleep(sleepMillis)
        }
    }

    synchronized int transfer(CreateTransfer request) {
        while (true) {
            try {
                if (running && currentQueue) {
                    return currentQueue.transfer(request, false)
                }
                else {
                    log.trace("No connection yet for 'transfer' message, retrying in ${sleep} seconds")
                }
            }
            catch (Exception e) {
                running = false
                log.trace("Action 'transfer' failed due to ${e.getMessage()}, retrying in ${sleep} seconds")
            }
            Thread.sleep(sleepMillis)
        }
    }

    def start() {
        this.running = true
    }

    def stop() {
        this.running = false
    }
}
