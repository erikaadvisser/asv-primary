package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState

/**
 * This is the state where a node disconnected during running, but it is not the primary node.
 *
 * Secondary version, the secondary waits for the primary to signal the new state.
 **/
class CSS_RecoverSecondaryFailure extends ClusterState {

    ClusterManager mgr
    int disconnectedNodeNumber

    CSS_RecoverSecondaryFailure(ClusterManager clusterManager, int disconnectedNodeNumber) {
        this.mgr = clusterManager
        this.disconnectedNodeNumber = disconnectedNodeNumber
    }

    def go() {
        def disconnectedNode = mgr.remoteNodes[disconnectedNodeNumber]
        disconnectedNode.disconnect()
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message != ClusterMessage.LOSS_OF_SECONDARY_REQUEST_LAST_ACTION_ID) {
            throw new RuntimeException("Unexpected message ${message} ${parameters}")
        }
        int lastActionId = mgr.database.idCounter
        mgr.primaryNode.send(ClusterMessage.LOSS_OF_SECONDARY_RESPONSE_LAST_ACTION_ID, lastActionId)
    }

}
