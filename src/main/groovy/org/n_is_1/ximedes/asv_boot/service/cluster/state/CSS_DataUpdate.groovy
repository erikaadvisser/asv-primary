package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState

/**
 * This is the state where the cluster is syncing data with the primary (the primary has been previously established
 * by the node that has the most up-to-date data.
 *
 * Secondary version, the secondary waits for the primary to signal the new state.
 *
 * This state relies on data queues to do the work. No cluster management messages are exchanged, except for
 * a transition to new state at the end.
 **/
class CSS_DataUpdate extends ClusterState {

    CSS_DataUpdate(ClusterManager ignored) {
    }

    def go() {
        // nothing to do
    }

}
