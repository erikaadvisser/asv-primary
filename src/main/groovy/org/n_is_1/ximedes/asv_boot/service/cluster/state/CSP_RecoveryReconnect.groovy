package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.NodeState
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

/**
 * This is the state where the cluster is accepting a previously lost node back into the cluster by
 * establishing connections to it
 *
 * Primary version, the primary manages transition to the next state.
 */
class CSP_RecoveryReconnect extends ClusterState {

    ClusterManager mgr

    CSP_RecoveryReconnect(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        def reconnectingNode = mgr.remoteNodes.values().find{ it.state == NodeState.DISCONNECTED }
        def secondaryNode = mgr.remoteNodes.values().find{ it.state != NodeState.DISCONNECTED }
        reconnectingNode.connect()
        reconnectingNode.state = NodeState.CONNECTED
        mgr.routerQueue.stop()

        secondaryNode.send(ClusterMessage.CLUSTER_STATE, CSS_RecoveryReconnect.simpleName)
        // no broadcast here, it would also send this message to the reconnecting node.
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message == ClusterMessage.CONNECTED_TO_LOST_NODE) {
            def newState = new CSP_NegotiatePrimary(mgr)
            mgr.updateStateAndGo(newState)
            return
        }
        throw new RuntimeException("Unexpected message ${message} ${parameters} from ${remoteNodeNumber}")
    }
}
