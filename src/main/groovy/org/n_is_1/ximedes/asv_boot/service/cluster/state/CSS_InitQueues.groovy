package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState

/**
 * This is the state where the nodes initialize their queues based on the primary
 *
 * Secondary version, the secondary waits for the primary to signal the new state.
 **/
class CSS_InitQueues extends ClusterState {

    ClusterManager mgr

    CSS_InitQueues(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.queueManager.initQueues(mgr.remoteNodes)
        mgr.primaryNode.send(ClusterMessage.READY)
    }

}
