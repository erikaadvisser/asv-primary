package org.n_is_1.ximedes.asv_boot.service.util

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

/**
 * This class keeps track of who is the primary of the cluster
 */
@Service
class PrimaryConfig {

    private @Value('${HOST_NUMBER}') int myHostNumber

    @Autowired Config config

    public int primaryHostNumber = 0
    public int fallbackPrimary = 1

    public boolean amIPrimary() {
        return myHostNumber == primaryHostNumber
    }

}
