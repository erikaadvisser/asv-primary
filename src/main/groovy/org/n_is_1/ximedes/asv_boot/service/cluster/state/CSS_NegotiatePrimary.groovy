package org.n_is_1.ximedes.asv_boot.service.cluster.state

import groovy.json.JsonSlurper
import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState
import org.n_is_1.ximedes.asv_boot.service.cluster.state.CSP_InitQueues

/**
 * This is the state where the nodes initialize their queues based on the primary
 *
 * Secondary version, the action is initially handled by the primary, but this node can become the new primary.
 **/
class CSS_NegotiatePrimary extends ClusterState {

    ClusterManager mgr

    CSS_NegotiatePrimary(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.allNodes.values().each { log.trace "node ${it.hostNumber} status: ${it.state}"}
        int lastActionId = mgr.database.idCounter
        mgr.primaryNode.send(ClusterMessage.LAST_ACTION_ID, lastActionId)
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message == ClusterMessage.YOU_ARE_KING) {
            def lastIds = processLastIdsJson(parameters[0])
            processPrimaryHandoverToMe(lastIds)
            return
        }
        if (message == ClusterMessage.I_AM_KING) {
            processNewPrimary(remoteNodeNumber)
            return
        }
        throw new RuntimeException("Unexpected message: ${message} ${parameters}")
    }

    private Map<Integer, Integer> processLastIdsJson(String jsonString) {
        def slurper = new JsonSlurper()
        def lastIdsAsStrings = (Map<String, Integer>) slurper.parseText(jsonString)
        Map<Integer, Integer> lastIds = [:]
        lastIdsAsStrings.each {
            int key = Integer.parseInt(it.key)
            lastIds[key] = it.value
        }
        log.trace("Cluster received json: ${lastIds}")
        return lastIds
    }

    void processNewPrimary(int newPrimaryNodeNumber) {
        mgr.updatePrimaryTo(newPrimaryNodeNumber)
        log.info("Cluster Acknowledge new primary node ${newPrimaryNodeNumber}")
    }

    private void processPrimaryHandoverToMe(Map<Integer, Integer> lastIds) {
        log.info("Cluster This node is taking over the role of primary.")
        mgr.updatePrimaryTo(mgr.config.myHostNumber)

        mgr.remoteNodes.values().each { it.send(ClusterMessage.I_AM_KING)}

        def newState = new CSP_InitQueues(mgr, lastIds)
        mgr.updateStateAndGo(newState)
    }
}
