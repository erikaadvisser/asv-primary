package org.n_is_1.ximedes.asv_boot.service.cluster.remote

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.slf4j.LoggerFactory

/**
 * This class receives cluster messages and passes them on to the ClusterManager
 */
class ClusterReceiver implements Runnable{

    def log = new Log()
    def logback = LoggerFactory.getLogger(ClusterReceiver)

    private DataOutputStream output
    private DataInputStream input

    private ClusterManager clusterManager

    private int remoteHostNumber

    ClusterReceiver(Socket socket, ClusterManager clusterManager) {
        output = new DataOutputStream(socket.getOutputStream())
        input = new DataInputStream(socket.getInputStream())
        this.clusterManager = clusterManager
    }

    int acceptHandshake() {
        String greeting = input.readUTF()
        remoteHostNumber = input.readInt()
        if ( greeting == "Cluster hello") {
            String response = "Cluster 5by5"
            if (clusterManager.primaryConfig.amIPrimary()) {
                response += " I am primary"
            }

            output.writeUTF(response)
        }
        else {
            throw new RuntimeException("Initial handshake failed, got '${greeting}', expected 'Cluster hello'.")
        }

        return remoteHostNumber
    }

    void run() {
        try {
            while (true) {
                String type = input.readUTF()
                handleMessage(type)
            }
        }
        catch (SocketException socketException) {
            log.warn("Lost cluster connection with node ${remoteHostNumber} (${socketException.getMessage()})")
            manageConnectionLoss()
            logback.debug("Lost cluster connection with node ${remoteHostNumber}", socketException)
        }
        catch (Exception exception) {
            logback.error("Fatal error occurred", exception)
        }
    }

    private manageConnectionLoss() {
        try {
            clusterManager.connectionLost(remoteHostNumber)
        }
        catch (Exception exception) {
            logback.warn("Failed to manage loss of connectoin to ${remoteHostNumber}", exception)
        }
    }

    def handleMessage(String type) {
        log.trace("Cluster received from node ${remoteHostNumber}: ${type} - retreiving parameters.")
        ClusterMessage message = ClusterMessage.valueOf(type)
        List<String> parameters = []
        for (int i = 0; i < message.parameterCount; i++) {
            String parameter = input.readUTF()
            parameters.add(parameter)
        }
        output.writeUTF("ack")

        log.debug("Cluster received from node ${remoteHostNumber}: ${message} ${parameters}")
        clusterManager.receiveMessage(remoteHostNumber, message, parameters)
    }
}
