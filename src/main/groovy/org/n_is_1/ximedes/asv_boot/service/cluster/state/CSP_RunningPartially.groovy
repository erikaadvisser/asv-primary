package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState

/**
 * This is the state where the partial cluster is running (2/3 nodes).
 *
 * Primary version, the primary manages transition to the next state (if possible).
 */
class CSP_RunningPartially extends ClusterState {

    ClusterManager mgr

    CSP_RunningPartially(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.routerQueue.start()
        mgr.broadcastClusterState(CSS_RunningPartially)
    }

    void connectionLost(int remoteNodeNumber) {
        mgr.routerQueue.stop()

        throw new RuntimeException("Loss of second node not supported.")
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message == ClusterMessage.CONNECTED) {
            def newState = new CSP_RecoveryReconnect(mgr)
            mgr.updateStateAndGo(newState)
            return
        }
        throw new RuntimeException("Unexpected message ${message} ${parameters}")
    }
}
