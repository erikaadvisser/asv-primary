package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterNode
import org.n_is_1.ximedes.asv_boot.service.data.Database
import org.n_is_1.ximedes.asv_boot.service.queue.ActionQueue
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer

import static org.n_is_1.ximedes.asv_boot.service.cluster.NodeState.DISCONNECTED

/**
 * This is the state where the cluster is syncing data with the primary (the primary has been previously established
 * by the node that has the most up-to-date data.
 *
 * Primary version, the primary manages transition to the next state
 */
class CSP_DataUpdate extends ClusterState {

    ClusterManager mgr

    int upNodeCount
    Map<Integer, Integer> lastIds = [:]

    CSP_DataUpdate(ClusterManager clusterManager, Map<Integer, Integer> lastIds) {
        this.mgr = clusterManager
        this.lastIds = lastIds
    }

    def go() {
        mgr.broadcastClusterState(CSS_DataUpdate)
        updateRemotes()
        def newState = determineNextState()
        mgr.updateStateAndGo(newState)
    }

    ClusterState determineNextState() {
        int upNodeCount = mgr.allNodes.values().count{ it.state != DISCONNECTED }
        if (upNodeCount == mgr.allNodes.size()) {
            return new CSP_RunningFully(mgr)
        }
        else {
            return new CSP_RunningPartially(mgr)
        }
    }

    private void updateRemotes() {
        int myLastId = mgr.database.idCounter

        log.trace("LastIds: ${lastIds} - class: ${lastIds.getClass()}")

        mgr.remoteNodes.values().each { ClusterNode remoteNode ->
            if (lastIds.containsKey(remoteNode.hostNumber)) {
                int remoteLastId = lastIds[remoteNode.hostNumber]
                if (myLastId != remoteLastId) {
                    updateRemote(remoteNode, myLastId, remoteLastId)
                }
            }
        }

        log.info("Cluster is in sync at id ${myLastId}.")
    }

    private void updateRemote(ClusterNode clusterNode, int myLastId, int theirLastId) {
        def startMillis = System.currentTimeMillis()


        log.info("Cluster node ${clusterNode.hostNumber} is out of sync, its last id is ${theirLastId}, but I am at ${myLastId}, updating now.")
        def queue = mgr.queueManager.remoteQueues[clusterNode.hostNumber]
        def db = mgr.database

        Account account

        int start = theirLastId + 1
        int count = 0

        for (int id = start; id <= myLastId; id++) {
            account = db.getAccount(id)
            if (account) {
                syncCreateAccount(queue, account, id, clusterNode.hostNumber)
            }
            else {
                syncTransfer(db, id, queue, clusterNode.hostNumber)
            }
            if (count % 1000 == 0) {
                log.debug("data updated ${count} actions.")
            }
            count++
        }


        def endMillis = System.currentTimeMillis()
        def seconds = (endMillis - startMillis) / 1000
        def tps = count / seconds
        log.info "Data update of ${clusterNode.hostNumber} took: ${seconds} seconds, ${tps} tps (for ${count} actions)"
    }

    private void syncTransfer(Database db, int id, ActionQueue queue, int nodeNumber) {
        Transfer transfer = db.getTransfer(id)
        log.trace("Updating transfer ${id} to ${nodeNumber}")

        if (!transfer) {
            throw new RuntimeException("Cluster FATAL no account or transfer found for id ${id}")
        }

        CreateTransfer request = new CreateTransfer(transfer.from, transfer.to, transfer.amount)
        int createdId = queue.transfer(request, true)
        checkIds(id, createdId)
    }

    private void syncCreateAccount(ActionQueue queue, Account account, int id, int nodeNumber) {
        log.trace("Updating create account ${id} to ${nodeNumber}")
        int createdId = queue.createAccount(account.overdraft, true)
        checkIds(id, createdId)
    }

    void checkIds(int localId, int remoteId) {
        if (localId != remoteId) {
            String msg = "Cluster FATAL - database inconsistency during update, local id: ${localId}, remoteId: ${remoteId}"
            log.error(msg)
            throw new RuntimeException(msg)
        }
    }
}
