package org.n_is_1.ximedes.asv_boot.service.queue.remote

import org.n_is_1.ximedes.asv_boot.service.data.DataManager
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.slf4j.LoggerFactory

/**
 * This class receives queue messages and passes them on to the DataManager
 */
class QueueReceiver implements Runnable {

    def log = new Log()
    def logback = LoggerFactory.getLogger(QueueReceiver)

    private DataOutputStream output
    private DataInputStream input

    private DataManager dataManager

    private int remoteHostNumber
    private Socket socket

    QueueReceiver(Socket socket, DataManager dataManager) {
        output = new DataOutputStream(socket.getOutputStream())
        input = new DataInputStream(socket.getInputStream())
        this.dataManager = dataManager
        this.socket = socket
    }

    int acceptHandshake() {
        String greeting = input.readUTF()
        remoteHostNumber = input.readInt()
        if ( greeting == "Hello") {
            output.writeUTF("Hi")
        }
        else {
            throw new RuntimeException("Initial handshake failed, got '${greeting}', expected 'Hi'.")
        }
        log.trace("Queue setup success, handshake ok from node ${remoteHostNumber}")

        return remoteHostNumber
    }

    void run() {
        try {
            while (true) {
                char c = input.readChar()

                if ( c == QueueMessageFormat.ACCOUNT) {
                    int overdraft = input.readInt()
                    boolean persistOnly = input.readBoolean()
                    int id = dataManager.createAccount(overdraft, persistOnly)
                    output.writeInt(id)
                    continue
                }
                if (c == QueueMessageFormat.TRANSFER) {
                    int from = input.readInt()
                    int to = input.readInt()
                    int amount = input.readInt()
                    boolean persistOnly = input.readBoolean()

                    CreateTransfer request = new CreateTransfer(from, to, amount)

                    int id = dataManager.transfer(request, persistOnly)
                    output.writeInt(id)
                    continue
                }
                if (c == QueueMessageFormat.TERMINATE) {
                    log.debug("Queue terminate at request of remote ${remoteHostNumber}")
                    socket.close()
                    return
                }
                throw new RuntimeException("Received unknown command from remote: '${c}'")
            }
        }
        catch (Exception e) {
            log.warn("Lost queue connection with host ${remoteHostNumber} (${e.getMessage()}")
            logback.debug("Lost queue connection with host ${remoteHostNumber}", e)
        }
    }
}
