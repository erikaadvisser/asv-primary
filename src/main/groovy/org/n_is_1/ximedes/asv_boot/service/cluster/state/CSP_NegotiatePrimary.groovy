package org.n_is_1.ximedes.asv_boot.service.cluster.state

import groovy.json.JsonOutput
import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterNode
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

import static org.n_is_1.ximedes.asv_boot.service.cluster.NodeState.DISCONNECTED

/**
 * This is the state where the cluster is establishing who is the best primary by comparing who has the latest
 * data change
 *
 * Primary version, the primary manages transition to the next state - the primary may change in this state.
 */
class CSP_NegotiatePrimary extends ClusterState {

    ClusterManager mgr

    int upNodeCount
    Map<Integer, Integer> lastIds = [:]

    CSP_NegotiatePrimary(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.allNodes.values().each { log.trace "node ${it.hostNumber} status: ${it.state}"}
        log.trace("Node state: ${JsonOutput.toJson(lastIds)}")
        upNodeCount = mgr.allNodes.values().count { it.state != DISCONNECTED }
        lastIds[mgr.myNode.hostNumber] = mgr.database.idCounter

        log.debug("Cluster there are ${upNodeCount} nodes up and running, asking them for their latest action id")
        mgr.broadcastClusterState(CSS_NegotiatePrimary)
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message == ClusterMessage.LAST_ACTION_ID) {
            int lastActionId = Integer.parseInt(parameters[0])
            processLastAction(remoteNodeNumber, lastActionId)
            return
        }

        if (message == ClusterMessage.I_AM_KING) {
            processIAmNoLongerPrimary(remoteNodeNumber)
            return
        }

        throw new RuntimeException("Unexpected message: ${message} ${parameters}")
    }

    private void processIAmNoLongerPrimary(int newPrimaryNodeNumber) {
        mgr.updatePrimaryTo(newPrimaryNodeNumber)
        log.info("Cluster Acknowledge new primary node ${newPrimaryNodeNumber}")
    }

    private void processLastAction(int remoteNodeNumber, int lastActionId) {
        lastIds[remoteNodeNumber] = lastActionId

        if (lastIds.size() == upNodeCount) {
            log.trace("Cluster all nodes submitted their last action id.")
            decideNextStep()
        }
        else {
            log.trace("Cluster waiting for information from other nodes.")
        }
    }

    private void decideNextStep() {
        int highestId = lastIds.values().max()

        if (mgr.database.idCounter == highestId) {
            // no need to switch to another primary, stick with this one.
            def newState = new CSP_InitQueues(mgr, lastIds)
            mgr.updateStateAndGo(newState)
        }else {
            int newPrimary = lastIds.find({it.value == highestId}).key
            ClusterNode newKing = mgr.allNodes[newPrimary]

            String param = JsonOutput.toJson(lastIds)
            newKing.send(ClusterMessage.YOU_ARE_KING, param)
        }
    }

}
