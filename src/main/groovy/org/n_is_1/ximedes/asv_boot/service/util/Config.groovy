package org.n_is_1.ximedes.asv_boot.service.util

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class manages the configuration over multiple hosts,
 *
 * this is useful to be able to run the cluster on a single machine, but also run it on multiple dockers.
 */
@Service
class Config {

    @Autowired Log log

    private @Value('${host1.store.path}') String host1_store_path
    private @Value('${host2.store.path}') String host2_store_path
    private @Value('${host3.store.path}') String host3_store_path

    private @Value('${host1.name}') String host1_name
    private @Value('${host2.name}') String host2_name
    private @Value('${host3.name}') String host3_name

    private @Value('${host1.queue.port}') int host1_queue_port
    private @Value('${host2.queue.port}') int host2_queue_port
    private @Value('${host3.queue.port}') int host3_queue_port

    private @Value('${host1.cluster.port}') int host1_cluster_port
    private @Value('${host2.cluster.port}') int host2_cluster_port
    private @Value('${host3.cluster.port}') int host3_cluster_port

    private @Value('${host1.operator.port}') int host1_operator_port
    private @Value('${host2.operator.port}') int host2_operator_port
    private @Value('${host3.operator.port}') int host3_operator_port

    public @Value('${HOST_NUMBER}') int myHostNumber

    public Map<Integer, String> hostNames = [:]
    public Map<Integer, Integer> queuePorts = [:]
    public Map<Integer, Integer> clusterPorts = [:]
    public Map<Integer, Integer> operatorPorts = [:]
    public def storePath = [:]

    @PostConstruct
    void init() {
        log.info "My host number is ${myHostNumber}"

        hostNames[1] = host1_name
        hostNames[2] = host2_name
        hostNames[3] = host3_name

        queuePorts[1] = host1_queue_port
        queuePorts[2] = host2_queue_port
        queuePorts[3] = host3_queue_port

        storePath[1] = host1_store_path
        storePath[2] = host2_store_path
        storePath[3] = host3_store_path

        clusterPorts[1] = host1_cluster_port
        clusterPorts[2] = host2_cluster_port
        clusterPorts[3] = host3_cluster_port

        operatorPorts[1] = host1_operator_port
        operatorPorts[2] = host2_operator_port
        operatorPorts[3] = host3_operator_port
    }

    int getMyQueuePort() {
        return queuePorts[myHostNumber]
    }

    int getMyClusterPort() {
        return clusterPorts[myHostNumber]
    }

    String getMyStorePath() {
        return storePath[myHostNumber]
    }

    int getMyOperatorPort() {
        return operatorPorts[myHostNumber]
    }

    boolean iAmOperator() {
        return myHostNumber == 0
    }

    /** If I am 1 then 2 is my right node. */
    int rightNodeHostNumber() {
        return cycle(myHostNumber)
    }

    /** If I am 1 then 3 is my left node. */
    int leftNodeHostNumber() {
        return cycle(cycle(myHostNumber))
    }

    /** Cycle to the next node number: 1->2 2->3 3->1 */
    private int cycle(int hostNumber) {
        int next = hostNumber +1
        return (next > 3) ? 1 : next
    }
}
