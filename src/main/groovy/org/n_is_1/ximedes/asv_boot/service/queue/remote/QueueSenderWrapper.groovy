package org.n_is_1.ximedes.asv_boot.service.queue.remote

import org.n_is_1.ximedes.asv_boot.service.queue.ActionQueue
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer

/**
 * Class to provide better insight in access to QueueSender
 */
class QueueSenderWrapper implements ActionQueue {

    private QueueSender sender

    QueueSenderWrapper(QueueSender sender) {
        this.sender = sender
    }

    int createAccount(int overdraft, boolean persistOnly) {
        return sender.createAccount(overdraft, persistOnly)
    }

    int transfer(CreateTransfer request, boolean persistOnly) {
        return sender.transfer(request, persistOnly)
    }

    void terminate() {
        sender.terminate()
    }

    void setup(String hostName, int port, int myHostNumber, int remoteHostNumber) throws Exception {
        sender.setup(hostName, port, myHostNumber, remoteHostNumber)
    }
}