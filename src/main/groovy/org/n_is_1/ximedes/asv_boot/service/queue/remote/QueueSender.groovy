package org.n_is_1.ximedes.asv_boot.service.queue.remote

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterNode
import org.n_is_1.ximedes.asv_boot.service.queue.ActionQueue
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer

/**
 * This class sends queue messages to a remote note
 */
class QueueSender implements ActionQueue {

    def log = new Log()

    private Socket socket
    private OutputStream outputBuffer
    private DataOutputStream output
    private DataInputStream input

    private int remoteHostNumber

    void setup(String hostName, int port, int myHostNumber, int remoteHostNumber) throws Exception {
        this.remoteHostNumber = remoteHostNumber

        socket = new Socket(hostName, port)
        outputBuffer = new BufferedOutputStream(socket.getOutputStream())
        output = new DataOutputStream(outputBuffer)
        input = new DataInputStream(socket.getInputStream())

        output.writeUTF("Hello")
        output.writeInt(myHostNumber)
        outputBuffer.flush()
        String response = input.readUTF()

        if (response != "Hi") {
            throw new RuntimeException("Handshake failed, connection failed.")
        }
        log.trace("Handshake succeeded, queue setup to node ${remoteHostNumber}")
    }

    synchronized int createAccount(int overdraft, boolean persistOnly) {
        log.trace("Sending bytes for createAccount to ${remoteHostNumber}")
        output.writeChar(QueueMessageFormat.ACCOUNT_INT)
        output.writeInt(overdraft)
        output.writeBoolean(persistOnly)
        outputBuffer.flush()
        int id = input.readInt()
        log.trace("Received back id ${id}")

        return id
    }

    synchronized int transfer(CreateTransfer request, boolean persistOnly) {
        log.trace("Sending bytes for transfer to ${remoteHostNumber}")
        output.writeChar(QueueMessageFormat.TRANSFER_INT)
        output.writeInt(request.from)
        output.writeInt(request.to)
        output.writeInt(request.amount)
        output.writeBoolean(persistOnly)
        outputBuffer.flush()
        int id = input.readInt()
        log.trace("Received back id ${id}")

        return id
    }

    synchronized void terminate() {
        try {
            output.writeChar(QueueMessageFormat.TERMINATE_INT)
            output.flush()
            socket.close()
        }
        catch (Exception ignore) {
            log.trace("Queue to remote ${remoteHostNumber} did not respond to termination request. Assuming already dead.")
        }
        log.debug("Queue to remote ${remoteHostNumber} terminated at our request.")
    }
}
