package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

import java.lang.management.ManagementFactory

/**
 * This is the state where the full cluster is running (all nodes).
 *
 * Secondary version, the secondary waits for the primary to signal the new state.
 **/
class CSS_RunningFully extends ClusterState {

    ClusterManager mgr

    CSS_RunningFully(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.routerQueue.start()
        ClusterStateUtil.logStartupTime()
    }


    void connectionLost(int remoteNodeNumber) {
        mgr.routerQueue.stop()

        if (remoteNodeNumber == mgr.primaryConfig.primaryHostNumber) {
            log.info("Cluster processing effects of node ${remoteNodeNumber} disconnecting - primaryNode.")
            mgr.primaryNode.disconnect()
            def newState = new CSS_RecoverPrimaryFailure(mgr)
            mgr.updateStateAndGo(newState)
        }
        else {
            log.info("Cluster processing effects of node ${remoteNodeNumber} disconnecting - secondary node.")
            def newState = new CSS_RecoverSecondaryFailure(mgr, remoteNodeNumber)
            mgr.updateStateAndGo(newState)
        }
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message == ClusterMessage.LOSS_OF_SECONDARY_REQUEST_LAST_ACTION_ID) {
            int lostNodeNumber = Integer.parseInt(parameters[0])

            log.info("Cluster being notified of connection loss to secondary, changing state")
            def newState = new CSS_RecoverSecondaryFailure(mgr, lostNodeNumber)
            mgr.updateStateAndGo(newState)
            mgr.receiveMessage(remoteNodeNumber, message, parameters)
            return
        }

        if (message == ClusterMessage.THE_KING_IS_DEAD) {
            log.info("Cluster being notified of connection loss to primary, changing state")
            mgr.primaryNode.disconnect()
            mgr.updatePrimaryTo(remoteNodeNumber)
            def newState = new CSS_RecoverPrimaryFailure(mgr)
            mgr.updateStateAndGo(newState)
            return
        }

        throw new RuntimeException("Unexpected message ${message} ${parameters}")
    }
}
