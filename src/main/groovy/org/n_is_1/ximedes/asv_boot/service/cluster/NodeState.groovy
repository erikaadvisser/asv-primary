package org.n_is_1.ximedes.asv_boot.service.cluster

/**
 * Defines the states a node can be in.
 */
enum NodeState {
    DISCONNECTED,
    CONNECTED,
    READY,
    RUNNING,
    UPDATING

}
