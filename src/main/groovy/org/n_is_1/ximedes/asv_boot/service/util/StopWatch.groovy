package org.n_is_1.ximedes.asv_boot.service.util

/**
 * Utility class for tracking time spent
 */
class StopWatch {

    long start = System.currentTimeMillis()
    int actions = 0

    def time() {
        long end = System.currentTimeMillis()

        long diff = end - start
        int seconds = diff / 1000
        int millis = diff - 1000 * seconds

        return "${seconds}.${millis}"
    }

    def tps() {
        long end = System.currentTimeMillis()

        long diff = end - start

        int tps = 1000 * actions / (diff + 1)

        return "${tps}"
    }

    def setActions(int a) {
        actions = a
    }
}
