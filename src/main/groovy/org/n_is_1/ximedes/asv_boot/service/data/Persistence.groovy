package org.n_is_1.ximedes.asv_boot.service.data

import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.service.util.StopWatch
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class implements the actual persistence: storing events to disk.
 */
@Service
class Persistence {

    static def logback = LoggerFactory.getLogger(Persistence)
    static def log = new Log()

    @Autowired Config config

    DataOutputStream stream

    @PostConstruct
    def init() {
        if (config.iAmOperator()) {
            return
        }

        String serFilePath = config.getMyStorePath()
        def file = new File(serFilePath)
        stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file, true)))
        log.trace("Using ser file: ${serFilePath}")
    }

    public flush() {
        stream.flush()
    }


    def saveAccount(Account account) {
        stream.writeUTF("a")
        stream.writeInt(account.id);
        stream.writeInt(account.overdraft);
        stream.writeInt(account.balance);

        log.trace("saved account ${account.id}")
    }

    def saveTransfer(Transfer transfer) {
        stream.writeUTF("t")
        stream.writeInt(transfer.id);
        stream.writeInt(transfer.from);
        stream.writeInt(transfer.to);
        stream.writeInt(transfer.amount);
        stream.writeByte(transfer.status.ordinal());

        log.trace("saved transfer ${transfer.id}")
    }

    Account loadAccount(DataInputStream dataIn) {
        int id, overdraft, balance
        id = dataIn.readInt()
        overdraft = dataIn.readInt()
        balance = dataIn.readInt()
        return new Account(id: id,
                overdraft: overdraft,
                balance: balance)
    }

    Transfer loadTransfer(DataInputStream dataIn) {
        int id, from, to, amount, statusOrdinal

        id = dataIn.readInt()
        from = dataIn.readInt()
        to = dataIn.readInt()
        amount = dataIn.readInt()
        statusOrdinal = dataIn.readByte()

        return new Transfer(id: id,
                from: from,
                to: to,
                amount: amount,
                status: Transfer.Status.values()[statusOrdinal])
    }

    def streamLoad(Database database) {
        def sw = new StopWatch()
        log.debug "Start loading database"

        String serFilePath = config.getMyStorePath()
        def file = new File(serFilePath)
        if (!file.exists()) {
            log.info "No old state to read, starting FRESH."
            return
        }

        def dataIn = new DataInputStream(new BufferedInputStream(new FileInputStream(file)))

        while (dataIn.available() > 0) {
            try {
                loadAction(dataIn, database)
            }
            catch (Exception exception) {
                log.info("Loading encountered problem: ${exception.getMessage()}")
                if (exception instanceof  EOFException) {
                    log.info("Unexpected end of file, the previous write did not finish successfully. Recovering.")
                }
                else {
                    logback.error("Unexpected problem loading from disk.", exception)
                }

                dataIn.close()
                dataIn = null
                recoverFromFaildLoad(database)
                break
            }
        }

        dataIn?.close()

        sw.setActions(database.idCounter)
        log.info ""
        log.info "Database loaded to id ${database.idCounter} at ${sw.tps()} tps. Took: ${sw.time()}"
    }

    def recoverFromFaildLoad(Database database) {
        log.info("Reconstructing the data file on disk.")
        clean()

        def sw = new StopWatch()

        int last = database.idCounter
        Account account
        Transfer transfer

        for (int id = 1; id <= last; id++) {
            account = database.getAccount(id)
            if (account) {
                saveAccount( account)
            }
            else {
                transfer = database.getTransfer(id)
                saveTransfer(transfer)
            }
        }
        stream.flush()
        log.info("Reconstructed the data file on disk, took ${sw.time()}")
    }

    private void loadAction(DataInputStream dataIn, Database database) {
        String indicator = dataIn.readUTF()

        if (indicator == "a") {
            def account = loadAccount(dataIn)
            database.accounts[account.id] = account
            if (account.id > database.idCounter) {
                database.idCounter = account.id
            }

        }
        else if (indicator == "t") {
            def transfer = loadTransfer(dataIn)
            database.transfers[transfer.id] = transfer
            if (transfer.id > database.idCounter) {
                database.idCounter = transfer.id
            }

            if (transfer.status == Transfer.Status.CONFIRMED) {
                Account from = database.accounts[transfer.from]
                Account to = database.accounts[transfer.to]
                from.balance -= transfer.amount
                to.balance += transfer.amount
            }
        }
        else {
            log.error "unknonw entry in stream ${indicator}"
            throw new RuntimeException()
        }
    }

    def clean() {
        this.stream.close()
        String serFilePath = config.getMyStorePath()
        def file = new File(serFilePath)
        if (file.exists()) {
            boolean deleteSuccess = file.delete()
            if (!deleteSuccess) {
                throw new RuntimeException("Failed to remove old db file ${file.getAbsolutePath()}")
            }
        }
        stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file, false)))
    }
}
