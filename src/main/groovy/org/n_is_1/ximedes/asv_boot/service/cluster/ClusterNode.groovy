package org.n_is_1.ximedes.asv_boot.service.cluster

import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterSender
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log

/**
 * Represents the ClusterManagers view of a single remote node
 */
class ClusterNode {



    static def log = new Log()

    final Config config
    final int hostNumber
    NodeState state = NodeState.DISCONNECTED

    private ClusterSender sender

    ClusterNode(Config config, nodeNumber) {
        this.config = config
        this.hostNumber = nodeNumber
    }

    void disconnect() {
        state = NodeState.DISCONNECTED
        if (sender) {
            sender.gracefullyCloseSocket()
            sender = null
        }
    }

    void send(ClusterMessage message, Object... args) {
        if (sender) {
            sender.send(message, args)
        }
        else {
            log.trace("No sender for node ${hostNumber}, skipping message: ${message} ${args}")
        }
    }

    boolean connect() {
        String hostName = config.hostNames[hostNumber]
        int port = config.clusterPorts[hostNumber]

        this.sender = new ClusterSender()
        boolean remoteClaimsPrimary = sender.setup(hostName, port, config.myHostNumber, hostNumber)

        return remoteClaimsPrimary
    }
}
