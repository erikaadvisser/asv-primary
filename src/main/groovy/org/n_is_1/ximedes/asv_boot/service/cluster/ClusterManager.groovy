package org.n_is_1.ximedes.asv_boot.service.cluster

import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState
import org.n_is_1.ximedes.asv_boot.service.cluster.state.CSA_StartingUp
import org.n_is_1.ximedes.asv_boot.service.data.Database
import org.n_is_1.ximedes.asv_boot.service.queue.RouterQueue
import org.n_is_1.ximedes.asv_boot.service.util.PrimaryConfig
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.service.util.QueueManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

import static org.n_is_1.ximedes.asv_boot.service.cluster.NodeState.*

/**
 * This class manages the cluster (synchronized start, primary handover, node recovery)
 */
@Service
class ClusterManager {

    def logback = LoggerFactory.getLogger(ClusterManager)
    def log = new Log()

    @Autowired Config config
    @Autowired PrimaryConfig primaryConfig
    @Autowired QueueManager queueManager
    @Autowired RouterQueue routerQueue
    @Autowired Database database

    Map<Integer, ClusterNode> allNodes = [:]
    Map<Integer, ClusterNode> remoteNodes = [:]

    ClusterNode myNode
    ClusterNode primaryNode

    ClusterState state = null

    @PostConstruct
    synchronized void init() {
        if (config.iAmOperator()) {
            log.info("I am an operator node.")
            return
        }


        int myNumber = config.myHostNumber

        myNode = new ClusterNode(config, myNumber)
        allNodes[myNumber] = myNode

        int leftNumber = config.leftNodeHostNumber()
        def leftNode = new ClusterNode(config, leftNumber)
        allNodes[leftNumber] = leftNode
        remoteNodes[leftNumber] = leftNode

        int rightNumber = config.rightNodeHostNumber()
        def rightNode = new ClusterNode(config, rightNumber)
        allNodes[rightNumber] = rightNode
        remoteNodes[rightNumber] = rightNode

        def state = new CSA_StartingUp(this, config)
        updateStateAndGo(state)
    }

    synchronized void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message == ClusterMessage.CLUSTER_STATE) {
            String simpleClassName = parameters[0]
            String fullClassName = "${ClusterState.class.package.name}.${simpleClassName}"

            ClusterState newState = (ClusterState)this.class.classLoader.loadClass( fullClassName ).newInstance(this)
                updateStateAndGo(newState)
        }
        else {
            state.receiveMessage(remoteNodeNumber, message, parameters)
        }
    }

    // -------------

    def synchronized updateStateAndGo(ClusterState newState) {
        this.state = newState
        log.info("Cluster changed state to: ${newState.getClass().simpleName}")
        try {
            newState.go()
        }
        catch(Exception e) {
            logback.error("Fatal failure in cluster state handling", e)
        }
    }

    public void broadcastClusterState(Class stateClass) {
        String param = stateClass.simpleName
        remoteNodes.values().each {
            it.send(ClusterMessage.CLUSTER_STATE, param)
        }
    }

    public boolean allConnectedNodes(NodeState expectedState) {
        return allNodes.values().every { it.state == expectedState || it.state == DISCONNECTED }
    }

    public boolean allNodes(NodeState expectedState) {
        return allNodes.values().every { it.state == expectedState }
    }

    synchronized void connectionLost(int remoteNodeNumber) {
        state.connectionLost(remoteNodeNumber)
    }

    /** Update both this class and the primaryConfig */
    void updatePrimaryTo(int nodeNumber) {
        primaryConfig.primaryHostNumber = nodeNumber
        primaryNode = allNodes[nodeNumber]
        if (config.myHostNumber == nodeNumber) {
            log.debug("Cluster configuring myself as the primary.")
        }
        else {
            log.info("Cluster configuring primary: ${nodeNumber}")
        }
    }
}
