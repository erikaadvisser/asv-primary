package org.n_is_1.ximedes.asv_boot.service.queue

import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer

/**
 * Interface for the queue to send actions
 */
interface ActionQueue {

    int createAccount(int overdraft, boolean persistOnly)

    int transfer(CreateTransfer request, boolean persistOnly)

    void terminate()
}
