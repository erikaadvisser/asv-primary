package org.n_is_1.ximedes.asv_boot.service.cluster.remote

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class defines a thread that listens for incoming cluster connections to set up ClusterReceivers.
 */
@Service
class ClusterListener {

    static final def logback = LoggerFactory.getLogger(ClusterListener)

    Log log = new Log()
    @Autowired Config config
    @Autowired ClusterManager clusterManager


    @PostConstruct
    void afterPropertiesSet() {
        if (config.iAmOperator()) {
            return
        }

        Thread.start {
            int port = config.getMyClusterPort()

            ServerSocket serverSocket = new ServerSocket(port)
            try {
                while (true) {
                    log.debug "Cluster: listening on port ${port}"
                    def socket = serverSocket.accept()
                    establishConnection(socket)
                }
            }
            catch (Exception e) {
                logback.error("Cluster listener died", e)
            }
        }
    }

    private void establishConnection(Socket socket) {
        def worker = new ClusterReceiver(socket, clusterManager)
        int remoteHostNumber = worker.acceptHandshake()
        def thread = new Thread(worker)
        thread.start()
        log.info "Cluster: established incoming connection with host ${remoteHostNumber}."
    }
}
