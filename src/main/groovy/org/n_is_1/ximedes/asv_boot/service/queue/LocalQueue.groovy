package org.n_is_1.ximedes.asv_boot.service.queue

import org.n_is_1.ximedes.asv_boot.service.data.DataManager
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class implements the local queue, messages from the local node headed for the database.
 */
@Service
class LocalQueue implements ActionQueue {

    @Autowired DataManager dataManager
    @Autowired Config config

    synchronized int createAccount(int overdraft, boolean ignored_persistOnly) {
        return dataManager.createAccount(overdraft, false)
    }

    synchronized int transfer(CreateTransfer request, boolean ignored_persistOnly) {
        return dataManager.transfer(request, false)
    }

    void terminate() {
        throw new RuntimeException("Don't be silly, you can't terminate a local queue.")
    }
}
