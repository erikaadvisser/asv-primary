package org.n_is_1.ximedes.asv_boot.service.cluster.remote

import org.n_is_1.ximedes.asv_boot.service.util.Log

/**
 * This class sends cluster messages to a remote note
 */
class ClusterSender {

    private Log log = new Log()
    private Socket socket
    private DataOutputStream output
    private DataInputStream input

    int remoteHostNumber

    boolean setup(String hostName, int port, int myHostNumber, int remoteHostNumber) throws Exception {
        this.remoteHostNumber = remoteHostNumber

        this.socket = createSocketConnection(hostName, port)

        output = new DataOutputStream(socket.getOutputStream())
        input = new DataInputStream(socket.getInputStream())

        output.writeUTF("Cluster hello")
        output.writeInt(myHostNumber)
        String response = input.readUTF()

        if (!response.startsWith("Cluster 5by5")) {
            throw new RuntimeException("Handshake failed, connection failed with ${remoteHostNumber}")
        }

        log.info("Cluster: established outgoing connection with host ${remoteHostNumber}")

        return (response.endsWith("I am primary"));

    }

    Socket createSocketConnection(String hostName, int port) {
        while (true) {
            try {
                return new Socket(hostName, port)
            }
            catch (IOException ignored) {
                log.trace("Node ${remoteHostNumber} not yet available.")
                Thread.sleep(1000)
            }
        }

    }

    synchronized void send(ClusterMessage message, Object... args) {
        log.trace("Sending ${message} to ${remoteHostNumber} ${args}")

        output.writeUTF(message.toString())
        args.each { messageParameter ->
            output.writeUTF(messageParameter.toString())
        }
        String response = input.readUTF()

        if (response != "ack") {
            throw new RuntimeException("Cluster message to ${remoteHostNumber} did not give ack, but '${response}'.")
        }
    }

    synchronized void gracefullyCloseSocket() {
        try {
            socket.close()
        }
        catch (Exception ignore) {

        }
    }

}
