package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.NodeState
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

/**
 * This is the state where the cluster is accepting a previously lost node back into the cluster by
 * establishing connections to it
 *
 * Secondary version, the secondary waits for the primary to signal the new state.
 */
class CSS_RecoveryReconnect extends ClusterState {

    ClusterManager mgr

    CSS_RecoveryReconnect(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.routerQueue.stop()
        def node = mgr.remoteNodes.values().find{ it.state == NodeState.DISCONNECTED }
        node.connect()
        node.state = NodeState.CONNECTED
        mgr.primaryNode.send(ClusterMessage.CONNECTED_TO_LOST_NODE)
    }

}
