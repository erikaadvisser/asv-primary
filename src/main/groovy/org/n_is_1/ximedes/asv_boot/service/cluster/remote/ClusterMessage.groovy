package org.n_is_1.ximedes.asv_boot.service.cluster.remote

/**
 * These are the cluster messages
 */
enum ClusterMessage {

    CONNECTED(0), // to primary     : the node has establish cluster connections to all other nodes
    READY(0),     // from secondary : the node is ready to start
    LAST_ACTION_ID(1), // from secondary: this is my last action id
    LOSS_OF_SECONDARY_REQUEST_LAST_ACTION_ID(1),   // from primary   : please send RESPONSE_LAST_ACTION_ID with the ID of your last action.
    LOSS_OF_SECONDARY_RESPONSE_LAST_ACTION_ID(1),  // from secondary : this is my last action ID.
    CLUSTER_STATE(1),  // from primary: the new cluster state is: x
    YOU_ARE_KING(1),    // from old primary: you are now the new primary. Param: json version of last_ids by node number.
    I_AM_KING(0),      // from new primary: claiming primary and telling nodes to update their primary reference.
    THE_KING_IS_DEAD(0), // from secondary that is taking the lead: the primary is not connected. The sender of this message will also be the temporary new primary.
    CONNECTED_TO_LOST_NODE(0) // from secondary: I have also connected to the lost (3rd) node.

    final int parameterCount

    ClusterMessage(int parameterCount) {
        this.parameterCount = parameterCount
    }
}
