package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.util.Log

import java.lang.management.ManagementFactory

/**
 * Contains methods shared between cluster states.
 */
class ClusterStateUtil {

    static def log = new Log()

    static boolean loggedStartupTime = false


    static void logStartupTime() {
        if (loggedStartupTime) {
            return
        }
        long jvmUpTime = ManagementFactory.getRuntimeMXBean().getUptime();
        int startupSeconds = Math.floor(jvmUpTime / 1000)
        int startupMillis = jvmUpTime - startupSeconds * 1000
        log.debug("Cluster startup time: ${startupSeconds}.${startupMillis}")
        loggedStartupTime = true
    }
}
