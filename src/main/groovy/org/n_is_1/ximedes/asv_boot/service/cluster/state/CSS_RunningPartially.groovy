package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.state.ClusterState

/**
 * This is the state where the partial cluster is running (2/3 nodes).
 *
 * Secondary version, the secondary waits for the primary to signal the new state.
 **/
class CSS_RunningPartially extends ClusterState {

    ClusterManager mgr

    CSS_RunningPartially(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.routerQueue.start()
    }

    void connectionLost(int remoteNodeNumber) {
        mgr.routerQueue.stop()

        throw new RuntimeException("Loss of second node not supported.")
    }
}
