package org.n_is_1.ximedes.asv_boot.service.data

import org.n_is_1.ximedes.asv_boot.service.queue.ActionQueue
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class manages that changes to the database are processed both locally and at remote nodes.
 *
 * In addition, it functions as another queue to the database, in order that all incoming queues requests are
 * handled one at a time.
 *
 * And finally it manages flushing of the persistent storage.
 */
@Service
class DataManager {

    def log = new Log()
    def logback = LoggerFactory.getLogger(DataManager)

    @Autowired Persistence persistence
    @Autowired Database database
    @Autowired Config config

    int hostNumber

    ActionQueue leftRemoteQueue
    ActionQueue rightRemoteQueue

    @PostConstruct
    void postConstruct() {
        if (config.iAmOperator()) {
            return
        }

        this.hostNumber = config.myHostNumber
        persistence.streamLoad(database)

        startFlushThread()
    }

    private Thread startFlushThread() {
        Thread.start {
            while (true) {
                try {
                    sleep(1 * 1000)
                    log.trace("Flushing database to disk")
                    persistence.flush()
                }
                catch (Exception e) {
                    logback.error("Flush thread", e)
                }
            }
        }
    }

    public synchronized int createAccount(int overdraft, boolean persistOnly) {
        if (persistOnly) {
            return saveAccountOnly(overdraft)
        }
        else {
            return createAccountAsPrimary(overdraft)
        }
    }

    private int createAccountAsPrimary(int overdraft) {
        int idLeft = createRemoteAccount(leftRemoteQueue, overdraft)
        int idRight = createRemoteAccount(rightRemoteQueue, overdraft)

        int remoteId = checkIds(idLeft, idRight)
        int localId = saveAccountOnly(overdraft)

        if (remoteId != localId) {
            throw new RuntimeException("Queue FATAL: the remotes have assigned different ids to the action ${remoteId} than the local db ${localId}")
        }
        return localId
    }

    int checkIds(int idLeft, int idRight) {
        if (idLeft == idRight) {
            if (idLeft == -1) {
                throw new RuntimeException("Both remote createAccounts failed due to connectivity problems." )
            }
            if (idLeft == 0) {
                throw new RuntimeException("No queue to send to, expected at least one queue." )
            }
            // happy flow: actual functional ids that are the same.
            return idLeft
        }

        // idLeft != idRight

        if (idLeft == -1 || idRight == -1 ) {
                log.trace("Connectivity problem during createAccount, but at least one remote node saved the action, continuing.")
            return Math.max(idLeft, idRight)
        }
        if (idLeft == 0 || idRight == 0 ) {
            // nothing to do, we only have one queue connected, that's ok.
            return Math.max(idLeft, idRight)
        }
        throw new RuntimeException("Queue FATAL: the remotes have assigne different ids to the action ${idLeft} vs ${idRight}")
    }

    int createRemoteAccount(ActionQueue actionQueue, int overdraft) {
        try {
            if (actionQueue) {
                return actionQueue.createAccount(overdraft, true)
            }
            else {
                log.trace("Skipping sending createAccount, because there is no queue to remote.")
                return 0
            }
        }
        catch (Exception exception) {
            log.info("Remote createAccount failed: ${exception.getMessage()}")
            logback.debug("Remote createAccount failed", exception)
            return -1
        }
    }

    private int saveAccountOnly(int overdraft) {
        return database.createAccount(overdraft)
    }

    public synchronized int transfer(CreateTransfer request, boolean persistOnly) {
        if (persistOnly) {
            return saveTransferOnly(request)
        }
        else {
            return transferAsPrimary(request)
        }
    }

    private saveTransferOnly(CreateTransfer request) {
        return database.transfer(request)
    }

    private int transferAsPrimary(CreateTransfer request) {
        int idLeft = transferRemote(leftRemoteQueue, request)
        int idRight = transferRemote(rightRemoteQueue, request)

        int remoteId = checkIds(idLeft, idRight)
        int localId = saveTransferOnly(request)

        if (remoteId != localId) {
            throw new RuntimeException("Queue FATAL: the remotes have assigned different ids to the action ${remoteId} than the local db ${localId}")
        }
        return localId
    }

    int transferRemote(ActionQueue actionQueue, CreateTransfer request) {
        try {
            if (actionQueue) {
                return actionQueue.transfer(request, true)
            }
            else {
                log.trace("Skipping sending transfer, because there is no queue to remote.")
                return 0
            }
        }
        catch (Exception exception) {
            log.info("Remote transfer failed: ${exception.getMessage()}")
            return -1
        }
    }

}
