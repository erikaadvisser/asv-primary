package org.n_is_1.ximedes.asv_boot.service.queue.remote

import org.n_is_1.ximedes.asv_boot.service.data.DataManager
import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class listens for connections to queues.
 */
@Service
class QueueListener implements Runnable {

    @Autowired Log log
    @Autowired Config hostManager
    @Autowired DataManager dataManager
    @Autowired Config config

    @PostConstruct
    void afterPropertiesSet() {
        if (config.iAmOperator()) {
            return
        }

        Thread listener = new Thread(this)
        listener.start()
    }

    void run() {
        int port = hostManager.getMyQueuePort()

        ServerSocket serverSocket = new ServerSocket(port)

        while (true) {
            log.debug "Queue: listening on port ${port}"
            def socket = serverSocket.accept()
            establishConnection(socket)
        }
    }

    private void establishConnection(Socket socket) {
        def worker = new QueueReceiver(socket, dataManager)
        int remoteHostNumber = worker.acceptHandshake()
        def thread = new Thread(worker)
        thread.start()
        log.debug "Queue: established incoming connection with node ${remoteHostNumber}."
    }
}
