package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterNode
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

/**
 * This is the state where a node disconnected during running, and it is the primary.
 *
 **/
class CSS_RecoverPrimaryFailure extends ClusterState {

    ClusterManager mgr

    CSS_RecoverPrimaryFailure(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.primaryNode.disconnect()

        ClusterNode otherSecondary = findOtherRemote()

        if (mgr.myNode.hostNumber < otherSecondary.hostNumber) {
            // I am the new temporary primary
            takeTheLead(otherSecondary)
        }
    }

    private void takeTheLead(ClusterNode otherSecondary) {
        log.info("Cluster I am the secondary with the lowest node number, taking the lead in recovery.")
        mgr.updatePrimaryTo(mgr.myNode.hostNumber)
        otherSecondary.send(ClusterMessage.THE_KING_IS_DEAD)
        def newState = new CSP_NegotiatePrimary(mgr)
        mgr.updateStateAndGo(newState)
    }

    private ClusterNode findOtherRemote() {
        int oldPrimaryNodeNumber = mgr.primaryNode.hostNumber
        ClusterNode otherSecondary = mgr.remoteNodes.find({ it -> it.key != oldPrimaryNodeNumber }).value
        otherSecondary
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message != ClusterMessage.THE_KING_IS_DEAD) {
            throw new RuntimeException("Unexpected message ${message} ${parameters}")
        }
        mgr.primaryNode.disconnect()
        mgr.updatePrimaryTo(remoteNodeNumber)

        log.trace("Ignoring notification that the primary is lost, already in correct recovery state.")
    }

}
