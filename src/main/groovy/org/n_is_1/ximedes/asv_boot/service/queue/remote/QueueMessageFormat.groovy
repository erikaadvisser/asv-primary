package org.n_is_1.ximedes.asv_boot.service.queue.remote

/**
 * This class defines message format. In essence it helps queues send/receive messages
 */
class QueueMessageFormat {

    public static final char ACCOUNT = 'a' as char
    public static final char TRANSFER = 't' as char
    public static final char TERMINATE = 'x' as char

    public static final int ACCOUNT_INT = (int)ACCOUNT
    public static final int TRANSFER_INT = (int)TRANSFER
    public static final int TERMINATE_INT = (int)TERMINATE

}
