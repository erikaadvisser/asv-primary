package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager

import java.lang.management.ManagementFactory

/**
 * This is the state where the full cluster is running (all nodes).
 *
 * Primary version, the primary manages transition to the next state (if possible).
 */
class CSP_RunningFully extends ClusterState {

    ClusterManager mgr

    CSP_RunningFully(ClusterManager clusterManager) {
        this.mgr = clusterManager
    }

    def go() {
        mgr.routerQueue.start()
        mgr.broadcastClusterState(CSS_RunningFully)
        ClusterStateUtil.logStartupTime()
    }


    void connectionLost(int remoteNodeNumber) {
        mgr.routerQueue.stop()
        log.info("Cluster processing effects of node ${remoteNodeNumber} disconnecting - secondary node.")

        def newState = new CSP_RecoverSecondaryFailure(mgr, remoteNodeNumber)
        mgr.updateStateAndGo(newState)
    }
}
