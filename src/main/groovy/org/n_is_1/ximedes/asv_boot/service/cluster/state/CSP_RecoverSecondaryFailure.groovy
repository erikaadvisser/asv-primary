package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

/**
 * This is the state where a node disconnected during running, but it is not the primary node.
 *
 * Primary version, the primary manages transition to the next state (if possible).
 */
class CSP_RecoverSecondaryFailure extends ClusterState {

    ClusterManager mgr
    int disconnectedNodeNumber

    CSP_RecoverSecondaryFailure(ClusterManager clusterManager, int disconnectedNodeNumber) {
        this.mgr = clusterManager
        this.disconnectedNodeNumber = disconnectedNodeNumber
    }

    def go() {
        def disconnectedNode = mgr.remoteNodes[disconnectedNodeNumber]
        disconnectedNode.disconnect()

        def activeNode = mgr.remoteNodes.values().find { it.hostNumber != disconnectedNodeNumber }
        activeNode.send(ClusterMessage.LOSS_OF_SECONDARY_REQUEST_LAST_ACTION_ID, disconnectedNodeNumber)

        mgr.queueManager.disableQueue(disconnectedNodeNumber)
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message != ClusterMessage.LOSS_OF_SECONDARY_RESPONSE_LAST_ACTION_ID) {
            throw new RuntimeException("Unexpected message ${message} ${parameters}")
        }

        int lastActionIdFromRemote = Integer.parseInt((parameters[0].toString()))

        if (mgr.database.idCounter == lastActionIdFromRemote) {
            log.info("Cluster confirmed all nodes at action id (local: ${mgr.database.idCounter}, remote: ${lastActionIdFromRemote})")
        }
        else {
            throw new RuntimeException("Cluster FATAL: unrecoverable discrepancy in action ids: " +
                    "(local: ${mgr.database.idCounter}, remote: ${lastActionIdFromRemote}).")
        }

        def state = new CSP_RunningPartially(mgr)
        mgr.updateStateAndGo(state)
    }
}
