package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterSender
import org.n_is_1.ximedes.asv_boot.service.util.Config

import static org.n_is_1.ximedes.asv_boot.service.cluster.NodeState.CONNECTED

/**
 * This is the state where the cluster is establishing cluster connections between all nodes.
 *
 * This class has no primary and secondary version, because when a node starts up it does not know
 * if it will be primary or secondary. Node 1 is the default primary, but only in case of a cold boot when
 * no other nodes are running. When node 1 previously left the cluster, another node took over primary. That
 * primary needs to be respected when node 1 starts up.
 */
class CSA_StartingUp extends ClusterState {

    ClusterManager mgr
    Config config

    int noteClaimingPrimary = 0

    // We only want to update the state once.
    boolean stateUpdated = false

    CSA_StartingUp(ClusterManager clusterManager, Config config) {
        this.mgr = clusterManager
        this.config = config
    }

    def go() {
        Thread.start {
            mgr.remoteNodes.values().each { node ->
                boolean remoteClaimsPrimary = node.connect()

                if (remoteClaimsPrimary) {
                    if (noteClaimingPrimary != 0) {
                        log.error("Two remote notes claim primary: ${node.hostNumber} and ${noteClaimingPrimary}." +
                                " Choosing the last one I asked: ${node.hostNumber}, and hoping for the best.")
                    }
                    noteClaimingPrimary = node.hostNumber
                }

            }
            mgr.myNode.state = CONNECTED

            decideOnPrimary()

            if (mgr.primaryConfig.amIPrimary()) {
                checkAllConnected()
            }
            else {
                mgr.remoteNodes.values().each { it.state = CONNECTED}
                mgr.primaryNode.send(ClusterMessage.CONNECTED)
            }
        }
    }

    /** Entry point could be the entire cluster startup, or a recovery of a node. */
    void decideOnPrimary() {
        if (noteClaimingPrimary == 0) {
            log.debug("Cluster detected cold start, letting the config decide the primary.")
            mgr.updatePrimaryTo(mgr.primaryConfig.fallbackPrimary)
        }
        else {
            log.debug("Cluster recovery detected, there are already nodes running.")
            mgr.updatePrimaryTo(noteClaimingPrimary)
        }
    }

    void receiveMessage(int hostNumber, ClusterMessage message, List<String> parameters) {
        if (message != ClusterMessage.CONNECTED) {
            throw new RuntimeException("Unexpected message: ${message} ${parameters}")
        }

        mgr.remoteNodes[hostNumber].state = CONNECTED
        checkAllConnected()
    }

    private synchronized void checkAllConnected() {
        if (stateUpdated) {
            return
        }

        if (mgr.allNodes(CONNECTED))  {
            stateUpdated = true
            def newState = new CSP_NegotiatePrimary(mgr)
            mgr.updateStateAndGo(newState)
        }
    }
}
