package org.n_is_1.ximedes.asv_boot.service.util

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterNode
import org.n_is_1.ximedes.asv_boot.service.cluster.NodeState
import org.n_is_1.ximedes.asv_boot.service.data.DataManager
import org.n_is_1.ximedes.asv_boot.service.queue.LocalQueue
import org.n_is_1.ximedes.asv_boot.service.queue.ActionQueue
import org.n_is_1.ximedes.asv_boot.service.queue.RouterQueue
import org.n_is_1.ximedes.asv_boot.service.queue.remote.QueueSender
import org.n_is_1.ximedes.asv_boot.service.queue.remote.QueueSenderWrapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class manages the queues.
 */
@Service
class QueueManager {

    @Autowired Log log
    @Autowired Config config
    @Autowired PrimaryConfig primaryConfig
    @Autowired LocalQueue localQueue
    @Autowired RouterQueue routerQueue
    @Autowired DataManager dataManager

    public Map<Integer, ActionQueue> remoteQueues = [:]

    void initQueues(Map<Integer, ClusterNode> nodes) {
        removeOldQueues()

        if (primaryConfig.amIPrimary()) {
            log.debug("Setting up local queue because I'm primary.")
            routerQueue.currentQueue = localQueue

            log.debug("Setting up data update queues to remotes .")
            dataManager.leftRemoteQueue = setupConnectionToLeft(nodes)
            dataManager.rightRemoteQueue = setupConnectionToRight(nodes)
        }
        else {
            log.debug("Setting up remote queue connection with primary.")
            routerQueue.currentQueue = setupRemote(primaryConfig.primaryHostNumber)
        }
    }

    private removeOldQueues() {
        dataManager.leftRemoteQueue?.terminate()
        dataManager.rightRemoteQueue?.terminate()

        dataManager.leftRemoteQueue = null
        dataManager.rightRemoteQueue = null

        if (routerQueue.currentQueue && !(routerQueue.currentQueue instanceof LocalQueue)) {
            routerQueue.currentQueue.terminate()
        }
        routerQueue.currentQueue = null

        remoteQueues = [:]
    }


    void disableQueue(int disconnectedNodeNumber) {
        int hostNumberLeft = config.leftNodeHostNumber()
        if (disconnectedNodeNumber == hostNumberLeft) {
            dataManager.leftRemoteQueue = null
        }
        else {
            dataManager.rightRemoteQueue = null
        }
    }

    private ActionQueue setupConnectionToLeft(Map<Integer, ClusterNode> nodes) {
        int hostNumber = config.leftNodeHostNumber()
        return setupConnectionToRemote(hostNumber, nodes)
    }

    private ActionQueue setupConnectionToRight(Map<Integer, ClusterNode> nodes) {
        int hostNumber = config.rightNodeHostNumber()
        return setupConnectionToRemote(hostNumber, nodes)
    }

    private ActionQueue setupConnectionToRemote(int nodeNumber, Map<Integer, ClusterNode> nodes) {
        if (nodes[nodeNumber].state == NodeState.DISCONNECTED) {
            return null
        }
        def sender = setupRemote(nodeNumber)
        log.trace("Remote queue connection established with remote host ${nodeNumber}.")
        return sender
    }


    private ActionQueue setupRemote(int hostNumber) {
        def sender = new QueueSenderWrapper(new QueueSender())


        String hostName = config.hostNames[hostNumber]
        int port = config.queuePorts[hostNumber]
        int myHostNumber = config.myHostNumber

        sender.setup(hostName, port, myHostNumber, hostNumber)

        remoteQueues[hostNumber] = sender
        return sender
    }

}
