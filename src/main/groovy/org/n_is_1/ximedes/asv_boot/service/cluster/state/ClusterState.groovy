package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage
import org.n_is_1.ximedes.asv_boot.service.util.Log

/**
 * The is the base class for the state machine of the cluster state.
 *
 * For convenience, this is not an interface but a null implementation where all calls result in exceptions.
 */
abstract class ClusterState {

    def log = new Log()

    /** Perform action at start of change to this state */
    abstract def go()

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        throw new RuntimeException("Unexpected call ${message} from ${remoteNodeNumber} in state ${getClass().simpleName}")
    }

    void connectionLost(int remoteNodeNumber) {
        throw new RuntimeException("Unrecoverable connection loss from ${remoteNodeNumber} for cluster in state ${getClass().simpleName}")
    }

}