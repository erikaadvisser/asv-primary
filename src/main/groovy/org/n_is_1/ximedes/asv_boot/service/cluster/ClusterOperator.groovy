package org.n_is_1.ximedes.asv_boot.service.cluster

import org.n_is_1.ximedes.asv_boot.service.util.Config
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class provides support for remote operation of the cluster
 */
@Service
class ClusterOperator {

    static def log = new Log()

    @Autowired Config config
    @Autowired ClusterManager clusterManager

    @PostConstruct
    def init() {
        if (!config.iAmOperator()) {
            new Thread().start {
                go()
            }
        }
    }


    def go() {
        int port = config.getMyOperatorPort()
        ServerSocket serverSocket = new ServerSocket(port)
        log.info("Cluster operator socked listening at: ${port}")

        while(true) {
            Socket socket = serverSocket.accept()
            try {
                DataInputStream input = new DataInputStream(socket.getInputStream())
                DataOutputStream output = new DataOutputStream(socket.getOutputStream())
                processConnection(input, output)
                socket.close()
            }
            catch (Exception exception) {
                log.warn("Failure processing operator command (${exception.getMessage()}")
            }
        }
    }

    def processConnection(DataInputStream input, DataOutputStream output) {
        String command = input.readUTF()

        switch (command) {
            case "status": return command_status(output)
            case "stop": return command_stop(output)
            case "start": return command_start(output)
            case "lastid": return command_lastid(output)
            case "kill": return command_kill()
            case "clean": return command_clean(output)
        }
    }

    def command_status(DataOutputStream output) {
        log.info("Received status command from operator.")
        String status = clusterManager.state.getClass().simpleName
        boolean primary = clusterManager.primaryConfig.amIPrimary()
        boolean running = clusterManager.routerQueue.running
        output.writeUTF(status)
        output.writeBoolean(primary)
        output.writeBoolean(running)
    }

    def command_stop(DataOutputStream output) {
        log.info("Received stop command from operator.")
        clusterManager.routerQueue.stop()
        output.writeUTF("ack")
    }

    def command_start(DataOutputStream output) {
        log.info("Received start command from operator.")
        clusterManager.routerQueue.start()
        output.writeUTF("ack")
    }

    def command_lastid(DataOutputStream output) {
        int lastid = clusterManager.database.idCounter
        log.info("Received lastid command from operator, responding with ${lastid}")
        output.writeInt(lastid)
    }

    def command_kill() {
        log.info("Received Kill command from operator, system.exit(1)")
        System.exit(1)
    }

    def command_clean(DataOutputStream output) {
        log.info("Received clean command from operator.")
        clusterManager.database.clean()
        output.writeUTF("ack")
    }
}
