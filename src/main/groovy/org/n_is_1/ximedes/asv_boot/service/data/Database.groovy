package org.n_is_1.ximedes.asv_boot.service.data

import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.service.util.Log
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * The main service class for all business actions
 */
@Service
class Database {


    int idCounter = 0

    HashMap<Integer, Account> accounts = new HashMap<Integer, Account>()
    HashMap<Integer, Transfer> transfers = new HashMap<Integer, Transfer>()

    @Autowired Log log
    @Autowired Persistence persistence

    synchronized int createAccount(int overdraft) {
        idCounter += 1

        def account = new Account(
                id: idCounter,
                overdraft: overdraft,
        )

        accounts.put(account.id, account)
        persistence.saveAccount(account)

        if (idCounter % 10000 == 0) {
            log.debug("save id = ${idCounter}")
        }

        return idCounter
    }

    synchronized int transfer(CreateTransfer request) {
        if (accounts[request.from] == null || accounts[request.to] == null) {
            return createTransfer(request, Transfer.Status.ACCOUNT_NOT_FOUND)
        }

        Account from = accounts[request.from]
        if (from.balance + from.overdraft < request.amount) {
            return createTransfer(request, Transfer.Status.INSUFFICIENT_FUNDS)
        }

        Account to = accounts[request.to]
        from.balance -= request.amount
        to.balance += request.amount

        return createTransfer(request, Transfer.Status.CONFIRMED)
    }

    private int createTransfer(CreateTransfer request, Transfer.Status status) {
        idCounter += 1

        def transfer = new Transfer(
                id: idCounter,
                from: request.from,
                to: request.to,
                amount: request.amount,
                status: status,
        )
        transfers[transfer.id] = transfer
        persistence.saveTransfer(transfer)

        if (idCounter % 10000 == 0) {
            log.debug("save id = ${idCounter}")
        }

        return idCounter
    }

    synchronized Account getAccount(int id) {
        return accounts[id]
    }

    synchronized Transfer getTransfer(int id) {
        return transfers[id]
    }

    synchronized def printBalance() {
        long total = 0;
        accounts.each { int id, Account account ->
            total += account.balance
        }
        log.info ""
        log.info "Accounts: ${accounts.size()}"
        log.info "Balance: ${total}"

        int successTransfers = 0;
        int failTransfers = 0;
        int otherTransfers = 0;

        transfers.each { int id, Transfer transfer ->
            if (transfer.status == Transfer.Status.CONFIRMED) {
                successTransfers += 1
            } else if (transfer.status == Transfer.Status.INSUFFICIENT_FUNDS) {
                failTransfers += 1
            } else {
                otherTransfers += 1
            }
        }
        log.info "Transfers: ${transfers.size()}"
        log.info "Success: ${successTransfers}"
        log.info "Failure: ${failTransfers}"
        log.info "Other: ${otherTransfers}"

        Set set = new HashSet()
        transfers.each { int id, Transfer transfer ->
            set.add(transfer)
        }
        log.info "Unique transfers ${set.size()}"

    }

    def synchronized clean() {
        persistence.clean()
        this.idCounter = 0

        this.accounts = new HashMap<Integer, Account>()
        this.transfers = new HashMap<Integer, Transfer>()
    }

}
