package org.n_is_1.ximedes.asv_boot.service.util

import org.springframework.stereotype.Service

/**
 * Mini logger
 */
@Service
class Log {

    final boolean trace = false
    final boolean debug = true
    final boolean info = true

    void trace(def message) {
        if (trace) {
            log("TRACE", message)
        }
    }

    void debug(def message) {
        if (debug) {
            log("DEBUG", message)
        }
    }

    void info(def message) {
        if (info) {
            log("INFO ", message)
        }
    }

    void warn(def message) {
        log("WARN ", message)
    }

    void error(def message) {
        log("ERROR", message)
    }

    private void log(def prefix, def message) {
        println "${prefix} ${message}"
    }
}
