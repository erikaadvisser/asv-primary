package org.n_is_1.ximedes.asv_boot.service.cluster.state

import org.n_is_1.ximedes.asv_boot.service.cluster.ClusterManager
import org.n_is_1.ximedes.asv_boot.service.cluster.remote.ClusterMessage

import static org.n_is_1.ximedes.asv_boot.service.cluster.NodeState.READY

/**
 * This is the state where the cluster is establishing cluster connections between all nodes.
 *
 * Primary version, the primary manages transition to the next state.
 */
class CSP_InitQueues extends ClusterState {

    ClusterManager mgr
    Map<Integer, Integer> lastIds = [:]

    CSP_InitQueues(ClusterManager clusterManager, Map<Integer, Integer> lastIds) {
        this.mgr = clusterManager
        this.lastIds = lastIds
    }

    def go() {
        mgr.queueManager.initQueues(mgr.remoteNodes)
        mgr.myNode.state = READY
        mgr.broadcastClusterState(CSS_InitQueues)
    }

    void receiveMessage(int remoteNodeNumber, ClusterMessage message, List<String> parameters) {
        if (message != ClusterMessage.READY) {
            throw new RuntimeException("Unexpected message: ${message} ${parameters}")
        }

        mgr.remoteNodes[remoteNodeNumber].state = READY
        checkAllReady()
    }

    void checkAllReady() {
        if (mgr.allConnectedNodes(READY))  {
            def newState = new CSP_DataUpdate(mgr, lastIds)
            mgr.updateStateAndGo(newState)
        }
    }
}
