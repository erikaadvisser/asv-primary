package org.n_is_1.ximedes;

import java.io.IOException;

import static org.n_is_1.ximedes.ClusterHelper.killNode;

/**
 * Starts the cluster
 */
public class Kill_Node_3 {

    public static void main(String... args) throws IOException {
        killNode(3);
    }

}
