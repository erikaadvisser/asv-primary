package org.n_is_1.ximedes;

import java.io.IOException;

import static org.n_is_1.ximedes.ClusterHelper.killNode;

public class Kill_Cluster {

    public static void main(String... args) throws IOException {
        killNode(1);
        killNode(2);
        killNode(3);
    }

}
