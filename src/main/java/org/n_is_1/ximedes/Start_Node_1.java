package org.n_is_1.ximedes;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.n_is_1.ximedes.ClusterHelper.startNode;

public class Start_Node_1 {

    public static void main(String... args) throws IOException {
        startNode(1);
    }
}
