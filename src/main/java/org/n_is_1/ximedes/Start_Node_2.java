package org.n_is_1.ximedes;

import java.io.IOException;

import static org.n_is_1.ximedes.ClusterHelper.startNode;

public class Start_Node_2 {

    public static void main(String... args) throws IOException {
        startNode(2);
    }
}
