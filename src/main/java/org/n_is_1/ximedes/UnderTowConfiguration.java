package org.n_is_1.ximedes;

import io.undertow.Undertow;
import org.springframework.boot.context.embedded.undertow.UndertowBuilderCustomizer;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class UnderTowConfiguration {

    @Bean
    public UndertowEmbeddedServletContainerFactory embeddedServletContainerFactory() {
        System.out.println("Undertow config");
        UndertowEmbeddedServletContainerFactory factory = new UndertowEmbeddedServletContainerFactory();
        factory.addBuilderCustomizers(new UndertowBuilderCustomizer() {

            @Override
            public void customize(Undertow.Builder builder) {
//                builder.addHttpListener(8080, "0.0.0.0");
                builder.setIoThreads(200);
                builder.setWorkerThreads(200);
            }

        });
        return factory;
    }
}
