package org.n_is_1.ximedes;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.Map;

/**
 * Sends messages to nodes of the cluster
 */
public class ClusterHelper {

    public static void killNode(final int node) throws IOException {
        Runnable killer = new Runnable() {
            public void run() {
                try {
                    int port = 8090 + node;
                    Socket socket = new Socket("localhost", port);
                    DataOutputStream out = new DataOutputStream(socket.getOutputStream());

                    out.writeUTF("kill");
                    socket.close();
                }
                catch (Exception ignore) {

                }
            }
        };

        Thread thread = new Thread(killer);
        thread.start();
    }


    public static void startNode(int nodeNumber) throws IOException {
        ProcessBuilder pb = new ProcessBuilder("java", "org.n_is_1.ximedes.Application");
        Map<String, String> env = pb.environment();
        env.put("CLASSPATH", "target/classes;lib/*");
        env.put("server.port", "808" + nodeNumber);
        env.put("HOST_NUMBER", "" + nodeNumber);
        File log = new File("out/node" + nodeNumber + ".log");
        pb.redirectErrorStream(true);
        pb.redirectOutput(ProcessBuilder.Redirect.appendTo(log));
        pb.start();
        System.out.println("Started node " + nodeNumber + "\n");
    }
}
