package simple

import util.BankClient

/**
 * This class implements a load test worker
 */
class LoadTestNode {

    BankClient client

    int bankAccountId
    ProgressIndicator indicator


    LoadTestNode(BankClient client, int bankAccountId, ProgressIndicator indicator) {
        this.client = client
        this.bankAccountId = bankAccountId
        this.indicator = indicator
        indicator.maxActions = 10 * 3 * 100 * 14
    }

    def go() {
        for (int i = 0; i < 10; i++ ) {
            Thread.start {
                new LoadTestSubThread().go()
            }
        }
    }

    class LoadTestSubThread {
        def go() {
            for (int tries = 0; tries < 3; tries++ ) {
                int merchantAccountId = client.createAccount(0);
                indicator.actions ++
                for (int consumerCount = 0; consumerCount < 100; consumerCount ++) {
                    int consumerAccountId = client.createAccount(0);
                    client.createTransfer(bankAccountId, consumerAccountId, 10)
                    indicator.actions ++
                    indicator.actions ++
                    for (int t = 0; t < 12; t++) {
                        client.createTransfer(consumerAccountId, merchantAccountId, 1)
                        indicator.actions ++
                    }
                }
            }
            indicator.done ++
        }
    }
}
