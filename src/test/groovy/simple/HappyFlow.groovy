package simple

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import util.BankClient

/**
 *
 */
class HappyFlow {


    static def main(def args) {
        new HappyFlow().go()
    }

    def go() {
        BankClient client = new BankClient()
        int id1 = client.createAccount(1000);
        int id2 = client.createAccount(1000);

        boolean success
        int transferId
        int balance, balance2

        transferId = client.createTransfer(id1, id2, 400)
        success = client.getTransfer(transferId)
        println "transfer ${(success)? 'success' : 'failure'}."

        balance = client.getAccountBalance(id1)
        println "Created account ${id1} that has balance ${balance}"
        balance2 = client.getAccountBalance(id2)
        println "Created account ${id2} that has balance ${balance2}"
    }


}
