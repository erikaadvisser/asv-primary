package simple

import util.BankClient

/**
 *
 */
class FullLoadTest {

    static int totalTrx = 420_301
//                        int totalTrx = 420_3001
//                        int totalTrx = 420_3010
//                        3_901_001


    static def main(def args) {
        long startMillis = System.currentTimeMillis()
        def tpsReport = new FullLoadTest().go()
        long endMillis = System.currentTimeMillis()

        long dif = endMillis - startMillis
        long seconds = Math.floor(dif/1000)
        long millis = dif - seconds * 1000
        println "Total time ${seconds}.${millis}"


        def warmTpsReport = tpsReport.subList(19, tpsReport.size())
        def tps = warmTpsReport.sum {it} / warmTpsReport.size()
        println "tps: ${tps}"
    }

    List<Integer> go() {

//        Start_Cluster.main()

//        Thread.sleep(1000 * 10)

        List<Integer> tpsReport = []

        BankClient client = new BankClient()
//        client.cleanDb(1,2,3)

        int bankAccountId = client.createAccount(10_000_000)

        List<ProgressIndicator> indicators = []
        for (int i = 0; i < 10; i++) {
            Thread.start {
                def indicator = new ProgressIndicator()
                indicators.add(indicator)
                new LoadTestNode(client, bankAccountId, indicator).go()
            }
        }

        println "Waiting for testrunners to start"
        Thread.sleep(1000 * 2)


        long startMillis = System.currentTimeMillis()
        long startActions = 0

        println "Running test"
        while (true) {
            if (indicators.findAll { it.isDone() }.size() == 10) {
                // all indicators at 100%
                client.stop()
                return tpsReport
            }
            int totalActions = indicators.sum { it.actions }

            long currentMillis = System.currentTimeMillis()
            long diff = currentMillis - startMillis


            int actionsNow = indicators.sum { it.actions }
            int trxDiff = actionsNow - startActions
            int tps = 1000 * trxDiff / diff
            tpsReport.add(tps)

            startMillis = System.currentTimeMillis()
            startActions = actionsNow

            int averageCompletion = indicators.sum { it.getPercent() } / 10;
            println "Test progress at ${averageCompletion} % - tps: ${tps} (trx total: ${totalActions}) [${diff} millis this cycle]"

            Thread.sleep(1000)
        }
    }
}
