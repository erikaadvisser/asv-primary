package simple

/**
 * Helper class that helps load test threads/nodes indicate their progress
 */
class ProgressIndicator {

    int actions
    int maxActions
    int done = 0

    int getPercent() {
        return (actions / maxActions) * 100
    }

    boolean isDone() {
        return done >= 10
    }
}
