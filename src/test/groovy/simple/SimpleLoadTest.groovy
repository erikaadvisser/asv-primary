package simple

import util.BankClient

/**
 *
 */
class SimpleLoadTest {

    static long startMillis

    static def main(def args) {
//        long startMillis = System.currentTimeMillis()
        new SimpleLoadTest().go()
        long endMillis = System.currentTimeMillis()

        long dif = endMillis - startMillis
        long seconds = Math.floor(dif/1000)
        long millis = dif - seconds * 1000
        println "Total time ${seconds}.${millis}"

        int transactions = 200_001
        int tps = transactions / seconds
        println "tps: ${tps}"
    }

    def go() {

        BankClient client = new BankClient()

        client.cleanDb(1,2,3)

        int bankId = client.createAccount(30_000_000);
        assert(bankId == 1)

        println "Created bank account."


        for (int t = 1; t <= 3; t++ ) {
            for (int i = 1; i <= 100; i++) {
                Thread.start {
                    int id = client.createAccount(0);
                    addAction()
                    for (int j = 1; j <= 1000; j++) {
                        int cid = client.createAccount(0);
                        addAction()
                    }
                }
            }
        }

        while( !finished()) {
            Thread.sleep(100)
        }
    }

    int actionCount = 0
    synchronized void addAction() {
        actionCount ++
        if (actionCount % 1000 == 0) {
            println ("at ${actionCount}")
            int temp = actionCount / 1000
            if ( temp == 100) {
                startMillis = System.currentTimeMillis()
            }
        }
    }
    synchronized boolean finished() {
        return actionCount >= 300_000
    }

}
