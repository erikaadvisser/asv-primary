package util

import groovy.transform.EqualsAndHashCode
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import org.slf4j.LoggerFactory

import java.util.concurrent.TimeUnit

/**
 * Provides methods to access the bank cluster
 */
class BankClient {

    private static def logback = LoggerFactory.getLogger(BankClient)
    private static def random = new Random()

    private static def BASE = "http://localhost:"

    private static Map<Integer, String> accountGet = [:]
    private static Map<Integer, String> accountCreate = [:]
    private static Map<Integer, String> transferGet = [:]
    private static Map<Integer, String> transferCreate = [:]
    private static Map<Integer, String> health = [:]
    private static Map<Integer, String> nodeBase = [:]
    private static final MediaType JSON = MediaType.parse("application/json");

    private OkHttpClient client
    private List<RemoteNode> availableNodes = Collections.synchronizedList([])
    private int upCount = 0
    private boolean running = true

    @EqualsAndHashCode
    class RemoteNode {
        int number
    }

    static {
        for (int i = 1; i <= 3; i++) {
            accountCreate[i] = BASE + "808${i}/account"
            accountGet[i] = BASE + "808${i}/account/"
            transferCreate[i] = BASE + "808${i}/transfer"
            transferGet[i] = BASE + "808${i}/transfer/"
            health[i] = BASE + "808${i}/health"
            nodeBase[i] = BASE + "808${i}"
        }
    }

    private int fixedNode

    BankClient(int fixedNode = 0) {
        this.fixedNode = fixedNode
        client = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.MILLISECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .build();

        resetHealth()

        new Thread().start {
            while (running) {
                resetHealth()
                Thread.sleep(500)
            }
        }
    }

    def resetHealth() {
        for (int i = 1; i <=3; i++) {
            def node = new RemoteNode(number: i)
            boolean up = checkUp(i)
            if (up) {
                if (!availableNodes.contains(node)) {
                    availableNodes.add(node)
                    print "[+${i}]"
                }
            }
            else {
                if (availableNodes.contains(node)) {
                    availableNodes.remove(node)
                    print "[-${i}]"
                }
            }
            upCount = availableNodes.size()
        }
    }


    boolean checkUp(int node) {
        def url = health[node]
        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Response response = client.newCall(request).execute();
            response.close()
            return response.isSuccessful()
        }
        catch (Exception ignore) {
            return false
        }
    }



    def stop() {
        running = false
    }

    private int getActiveNode() {
        if (this.fixedNode) {
            return fixedNode
        }

        int index = random.nextInt(upCount)
        def node = availableNodes.get(index)
        return node.number
    }

    int createAccount(int overdraft) throws IOException {
        while (true) {
            try {
                return tryCreateAccount(overdraft)
            }
            catch (Exception ignore) {
                print "-"
            }
        }
    }

    private int tryCreateAccount(int overdraft) throws IOException {
        int node = getActiveNode()
        def url = accountCreate[node]
        def json = "{\"overdraft\":${overdraft}}"
        RequestBody body = RequestBody.create(JSON, json)
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();
        return extractLocation(response)
    }

    int getAccountBalance(int id) throws IOException {
        int node = getActiveNode()
        String url = accountGet[node] + id
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        String body = response.body().string();
        response.close()


        int idIndex = body.indexOf(':')
        int balanceStart = body.indexOf(':', idIndex + 1) +1
        int balanceEnd = body.indexOf(',', balanceStart)

        String balanceString = body.substring(balanceStart, balanceEnd)

        return Integer.parseInt(balanceString)
    }

    int createTransfer(int fromId, int toId, int amount) throws IOException {
        while (true) {
            try {
                return tryCreateTransfer(fromId, toId, amount)
            }
            catch (Exception ignore) {
                print "."
            }
        }
    }

    private int tryCreateTransfer(int fromId, int toId, int amount) {
        int node = getActiveNode()
        def url = transferCreate[node]
        def json = "{\"from\":\"${fromId}\",\"to\":\"${toId}\",\"amount\":${amount}}"
        RequestBody body = RequestBody.create(JSON, json)
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();
        return extractLocation(response)
    }

    boolean getTransfer(int id) {
        int node = getActiveNode()
        String url = transferGet[node] + id
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        String body = response.body().string();
        response.close()

        int statusStart = body.lastIndexOf(':')+2
        int statusEnd = body.indexOf('\"', statusStart)
        String status = body.substring(statusStart, statusEnd)

        return "CONFIRMED" == status
    }


    private int extractLocation(Response response) {
        def location = response.header("Location")
        response.close()

        int idStart = location.lastIndexOf('/') + 1
        String idString = location.substring(idStart)
        int id = Integer.parseInt(idString)
        return id
    }

    public void cleanDb(Integer... nodes) {

        nodes.each{ int node ->

            if (checkUp(node)) {
                send(node, "/clean")

                println "Clean cleared ${node}"
            }

        }
    }

    private void send(int node, String path) {
        String url = nodeBase[node] + path
        println "calling ${url}"

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new RuntimeException("Failed to call: ${path} on node ${node} : ${response.message()}")
        }
        response.close()
    }
}
