import groovy.json.JsonSlurper

/**
 *
 */
class MyTest {

    static int count = 0

    static def main(def args) {

        def random = new Random()
        def map = Collections.synchronizedMap([:])

        new Thread().start {

            while (true) {
                map[count] = count
                count++

                if (count % 1000 == 0) {
                    println "Count ${count}"
                }
                Thread.sleep(1)
            }
        }

        Thread.sleep(1000)

        while (true) {
            int r = random.nextInt(count-2) + 1
//            println "r = ${r}"

            int v = map[r]
            assert( v == r)
        }
    }
}
